# wuam

> dual wm

## Description

wuam is a window manager designed specifically for dual monitor setups.

The purpose is to take advantage of the real estate provided by 2 screens,
while keeping in mind that both screens offer visible space, instead of
operating each monitor as an independent container.

Windows are selected not by movement, like most other wm, but by **fixed keys**.
In these examples I will set said keys to `H, K, L, ;`. That is, pressing 
`mod + h` will focus the container labeled `H` in the screenshots, and so on.

![screenshot of 2 windows per screen with
keybinds](./images/H_J_left_K_L_right.png)

`H` and `J` focus windows in the left monitor, while `K` and `L` will focus the
windows on the right monitor.

**The priority of placing new windows follows these principles:**

No tiling window will spread across monitors, but when considering where to
place windows, the totality of both monitors will be taken into account.

**The first window is to be placed in the biggest space available**. For the purpose
of explaining I will use a concrete example: Biggest monitor on the left
(primary) and smallest on the right (secondary).

Given no set rules, **the first window will be opened on the primary monitor**,
occupying it fully. The second window will be opened in the secondary monitor.

![screenshot of both monitors occupied by a window
each](./images/1_left_2_right.png)

What happens to a third opened window will depend on the option `-n Number of
windows` detailed [below], but for now let's stick to a concrete example and
assume that `-n` is set to 2, which means **2 windows per screen**.

![screenshot of third window on the left monitor](./images/3_1_left_2_right.png)

So the third window will open in the primary monitor, taking the biggest space,
respecting the `ratio` set by the user.

![screenshot of 4th window on the right
monitor](./images/3_1_left_4_2_right.png)

Now both monitors are fully occupied, since `-n` is set to `2`.

So what happens when we open a new window?

![screenshot of windows displaced](./images/5_3_left_4_2_right.png)

Now, the oldest window in the stack (the history of opened windows and their
order) **is now hidden**, and the newest window occupies the biggest of the 4
slots.

Opening more programs will keep scrolling windows in this manner.

![screenshot of windows further displaced](./images/5_3_left_6_4_right.png)

Now you might say, 5 4 6 4? This is confusing, do we have to remember the order
in which the windows were opened?

No, the idea is that the newest window will take the biggest space available,
under the assumption that that's where your attention will be focused right
after you opened. The beauty of this system is that you don't need to remember
where things are, you just look at the screen and you know where to go, because
the keybinds will always be fixed:

![screenshot showing keybinds with 4 windows](./images/H_J_left_K_L_right.png)

It doesn't matter in which order they were opened, or what program they are, `H` will always focus the leftmost window, `J` the second leftmost, etc. **Having fixed binds for fixed locations will greatly aid muscle memory.**

To access hidden windows, there's a special bind. When called, **selection mode**
will be activated. It will display all hidden windows, and you can select them
by pressing single keys, preferably the same as you use for movement, such as
`J, K, L, ;`, and more as needed, focused around the home row (if there's 8
windows, then `A, S, D, F` would be added). Once a window is selected (or
multiple, by holding `shift` for the first window, and confirmed, by pressing
`Enter`), then this new window(s) will enter the bigest visible space, hiding the
oldest window in the stack.

[screenshot showing selection mode]

What's more, **windows per screen can be changed on the spot**.

So having a `1` layout with 2 hidden windows and switching it to a `2` layout
will bring up those windows.

## Usage

These are the basics for window placement and on-screen movements, there are
some features to deal with exceptions, such as ghost windows, ghost desktops,
and pinned windows.

**What are ghosts?**

Why are they called ghosts? Simple: They're floating and they hide. If you have
floating windows in a tiling wm, chances are you will want to hide them, and
show them, depending on what you're doing.

To keep matters simple, **wuam allows for 1 ghost window per screen**. The concept
is the same as a scratchpad from i3, it's a window that's floating and you can
toggle un/hide with a keybind. You can have only one window, which will show in
the focused desktop when called, or you can have one ghost window per monitor,
fixed to each. They can be swapped.

Choose in which direction the following window will be: left, up, down and
left. Now choose the *next* type of division, which will alternate.

For [example], left and up:

[screenshot displaying]

In this case, the 

Make a pool of rectangles from all monitors. 

No rectangle can ever cross monitors *when tiled*.

Select the biggest available rectangle from both monitors, in this case, the
entire biggest monitor:

[screenshot]

The next window will be placed on the *left*, it is advised that the direction
of new windows matches the order of the screens, so that they essentially
rotate in that direction.

## Options

-n Number of windows per screen

-p Pinned windows

-r Ratio

-d Directions X, Y

-g | -c Gravity|Carrousel

### Number of windows per screen:
Windows per screen: [1, 1.5, 2, 2.5, 3]


A [screenshot] is better than 1000 words.

But for those who want text:

1 window will open a maximum of 1 window per monitor.  When the max amount of
displayed monitors is achieved, hide the (in the example case) leftmost window,
move the smallest monitor's window to the bigger monitor, and spawn the new
window on the smallest monitor. Oldest windows will be hidden as more windows
are opened.

If you choose 2, then 1st window goes on biggest monitor, 2nd on the smallest,
now 3rd window next to 1st window, occupying the biggest slot, respecting the
ratio given by the user. 4th window now on the biggest node of the smallest
monitor. Now there are 4 windows displayed, 2 per monitor.

The next window will replace the biggest window, hiding what was previously there.

The user can move to any window by pressing the location they are on screen.

[screenshots]

When there are hidden windows a notification will be shown, through bar or window highlight color.

Floating windows get their own empty "ghost" workspace that is only, and
optionally automatically, displayed when it's parent monitor is focused.

That way, an image editor, such as GNU Image Manipulation Program, can display
it's floating windows neatly organized separate from the window displaying the
main window. In this case the ghost windows will be accessible by toggling
ghost mode on. You press the ghost mode bind, and the floating-windows
workspace appears. Now, as indicated by the bar, you are in ghost mode, and the
normal movement keys will apply to the floating window.

Once you press the ghost toggle keybind again, it is now off, and the keys will
refer back to the tiled monitor that was there before. You can swap main window
and ghost desktop from desktop to desktop.


### So what is floating point number?

/ A                       /
[screenshot of 1 window on left monitor, empty right monitor, A]

/ B                       /         / A                 /
[screenshot of 1 window per monitor, BA]

/ C                       /         / B         / A     /
[screenshot of 1 window on left screen, 2 windows on right screen, CBA]

/ D                       /         / C         / B     / 
[screenshot of 3 windows displaced, DCB]


At this point, the list can be called, but since it only contains A, it will
bring up A:


/ E                       /         / D         / C     / 
[1 window left, 2 windows right, EDC]

If it contained more than one window, then a list is called up, in which will
be displayed the windows available, which can be selected with the same buttons
to move around windows, and will be placed according to the schema.

/ AE                      /         / D         /C     /
[1 window left, 2 windows right, AE D C]

In the above, A was called back, and it took the place E previously occupied,
becoming E from now on.


I don't allow for more than 3. I don't have very big screens so I don't know
how to design for them. You can try your best to try to patch in higher window
count but I want you to know that nothing on my end is tested for more than 6
windows total.

Windows can be pinned, essentially fullscreening one app in a fixed monitor and
operating all new windows in the other.

Windows can be stickied, meaning they will remain in that monitor as long as
they're present. For example if your chat is set to the secondary monitor it
will remain there as 6 windows are opened, never move to the primary monitor
unless manually moved. When show again it will appear in the secondary monitor.

Windows can be sticky ghosts, meaning they will spawn, and remain when brought
back, remembering the last position and monitor. For example, a note-taking
file is opened in nvim in floating mode, it is called and hidden by the same
key combination. There can only be 2 sticky ghosts, one per monitor.

This wm is about simplifying what's hidden and focusing on
what's shown, and when it was called.

You can switch around the sticky ghosts, but you can't have
more than 1 per monitor at a time. They are not the same as
ghost desktops.

As you can see, there is an emphasis in hiding floating
windows, and toggle them on and off in specific ways,
reducing the complexity of interaction with things that are
not currently visible.

Some examples of good windows for sticky ghosts: music
player, note taking, quick audio recording, chat window,
file browser, man pages, quick terminal, calculator.

That's it, there are no "directions" as in vim-like arrows to move accross
windows or move them around.

To move them you enter move mode, in which you select the focused window's new
position, pressing the basic position keys.

For example:

/ A   / C         /  / D        /   B /
[screenshot of 4 windows, displaying ACDB, with gravity]
mod + a will select A
mod + b will select B, etc.

Instead of thinking in terms of ABCD, which imply an alphabetical order,
it is best to think of biggest and smallest.

for example, 
mod + j will focus J
mod + k will focus K
mod + h will focus H and
mod + l will focus L

mod + n will circulate the windows in the specified direction, 
if there is none hidden. If there are hidden, non sticky ghost windows, 
they will be entered into the circulation, the oldest window previously visible 
now taking its place among the hidden ones.

mod + mod2 (alt or shift) + n will cycle in the opposite direction.

/ H   / J       /   / K         / L   /
[screenshot of 4 windows with gravity, HJKL]

(in the example above, gravity is selected, as opposed to direction, the
biggest windows will gravitate towards the union of both screens)
